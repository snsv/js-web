function getFeeds(){
	function getBlogger(){
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
		    if (xhr.readyState == 4 && xhr.status == 200) {
		        var data = JSON.parse(xhr.responseText); 
		        if (data.status == 'ok') {    
			        $("#blg-title").html(data.items[0]['title']);
			        $("#blg-date").html(data.items[0]['pubDate']);
			        $("#blg-thumb").attr('style','background-image:url("'+ data.items[0]['thumbnail'].replace('s72-c','s1600') + '")') ;
			        $("#blg-short").html(data.items[0]['description'].split('<br>')[0]);

			        $("#blg-title2").html(data.items[1]['title']);
			        $("#blg-date2").html(data.items[1]['pubDate']);
			        $("#blg-thumb2").attr('style','background-image:url("'+ data.items[1]['thumbnail'].replace('s72-c','s1600') + '")') ;
			        $("#blg-short2").html(data.items[1]['description'].split('<br>')[0]);
		        }
		    }
		};
		xhr.open(
		    'GET',
		    'https://api.rss2json.com/v1/api.json?rss_url=https://blog.jsnow.co.uk//feeds/posts/default',
		    true
		);
		xhr.send();        
	}

	function getTwitter(){
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
		    if (xhr.readyState == 4 && xhr.status == 200) {
		        var data = JSON.parse(xhr.responseText);       
		        if (data.status == 'ok') {    
		        	$("#twt-date").html(data.items[0]['pubDate']);
		        	//$("#twt-thumb").attr('src',data.items[0]['thumbnail']);
		        	$("#twt-thumb").attr('style','background-image:url("'+ data.items[0]['thumbnail']+ '")');
			    	$("#twt-short").html(data.items[0]['title']);
			    	        	
    	        	$("#twt-date2").html(data.items[1]['pubDate']);
    	        	//$("#twt-thumb").attr('src',data.items[0]['thumbnail']);
    	        	$("#twt-thumb2").attr('style','background-image:url("'+ data.items[1]['thumbnail']+ '")');
    		    	$("#twt-short2").html(data.items[1]['title']);
		        }
		    }
		};
		xhr.open(
		    'GET',
		    'https://api.rss2json.com/v1/api.json?rss_url=https://www.twitrss.me/twitter_user_to_rss/?user=snsvim',
		    true
		);
		xhr.send();        
	}
	getBlogger();
	getTwitter();
}

	//$("#inserhere").html('test');	 


	//document.getElementsByClassName('post-date')[1].innerHTML = 'Success!';




/*
function grab_rss(url,index){
    var xmlhttp;        
    if (window.XMLHttpRequest)
      {// For IE7 and above, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// For IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.open("GET",url,false);
    xmlhttp.send();
    //xmlDoc=xmlhttp.responseXML;
    return xmlhttp.responseXML;
}  
//get bloggerXML feed
var bloggerXML = grab_rss("http://feeds.feedburner.com/snsvim")
postThumb = bloggerXML.getElementsByTagName("content")[0].textContent.split('src=')[1].split('"')[1]
postTitle = bloggerXML.getElementsByTagName("title")[1].textContent
postDateRaw = bloggerXML.getElementsByTagName("published")[0].textContent.split('.')[0]
postDate = (new Date(postDateRaw + 'Z')).toString().split(" ");
postDateString = postDate[1] + " " + postDate[2] + ", " + postDate[3]
postPreview = bloggerXML.getElementsByTagName("content")[0].textContent.split('"')[0];
//document.getElementsByClassName('post-image')[0].setAttribute("src", postThumb);
document.getElementsByClassName('post-image')[0].setAttribute("style", "background-image:url('" + postThumb + "')");
document.getElementsByClassName('post-title')[0].innerHTML = postTitle;
document.getElementsByClassName('post-date')[0].innerHTML = postDateString;
document.getElementsByClassName('post-preview')[0].innerHTML = postPreview; 
//get twitter feed
var twitterXML = grab_rss("https://www.twitrss.me/twitter_user_to_rss/?user=snsvim")
postThumb = twitterXML.getElementsByTagName("item")[0].textContent.split('src="')[1].split('"')[0]
postDateRaw = twitterXML.getElementsByTagName("pubDate")[0].textContent
postDate = postDateRaw.split(" ")
postDateString = postDate[2] + " " + postDate[1] + ", " + postDate[3]
//postContent = twitterXML.getElementsByClassName("TweetTextSize")[0].textContent

document.getElementsByClassName('post-image')[1].setAttribute("style", "background-image:url('" + postThumb + "')");
//document.getElementsByClassName('post-preview')[1].innerHTML = postPreview;
document.getElementsByClassName('post-date')[1].innerHTML = postDateString;
*/